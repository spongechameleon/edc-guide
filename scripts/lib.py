import sys

def execute(fn):
    """
    Assumes that the first CLI argument is the read file, the second CLI argument is the write file
    """

    with open(sys.argv[1], 'r') as f, open(sys.argv[2], 'w') as f_tmp:
        string = fn(f)
        f_tmp.write(string)

def find_tag_indices(string, tag, start_idx=0):
    """
    Returns (opening_tag_start_index, opening_tag_end_index, closing_tag_start_index, closing_tag_end_index)

    If no tag is found, returns (-1, -1, -1, -1)
    """

    try:
        opening_start_idx = string.index('<' + tag, start_idx)
        opening_end_idx = string.index('>', opening_start_idx)
        closing_start_idx = string.index('</' + tag + '>', start_idx)
        closing_end_idx = closing_start_idx + len(tag) + 2 # 2 to include the '/' and '>' chars
        return (opening_start_idx, opening_end_idx, closing_start_idx, closing_end_idx)
    except ValueError:
        return (-1, -1, -1, -1)

