import lib

def add_target_blank_to_a_tags(file):
    target_blank = 'target="_blank"'
    string = file.read()
    last_idx = 0

    while last_idx < len(string):
        (a, b, _, _) = lib.find_tag_indices(string, 'a', last_idx)

        if a == -1:
            return string

        substring = string[a:b + 1]
        t = substring.find(target_blank)

        if t == -1:
            string = string[:b] + " " + target_blank + string[b:]

        last_idx = b+1

    return string

lib.execute(add_target_blank_to_a_tags)

