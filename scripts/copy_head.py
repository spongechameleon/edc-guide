import sys, lib

def copy_head(styles_file, content_file):
    """
    Styles file: get everything from the beginning of the file through '</head>'

    Content file: get everything after '</head>' until the end of the file

    Return the combined string
    """

    styles_string = styles_file.read()
    content_string = content_file.read()

    (_, _, _, styles_end_idx) = lib.find_tag_indices(styles_string, 'head')
    (_, _, _, content_start_idx) = lib.find_tag_indices(content_string, 'head')

    return styles_string[:styles_end_idx+1] + content_string[content_start_idx+1:]

with open(sys.argv[1], 'r') as styles_file, open(sys.argv[2], 'r') as content_file, open(sys.argv[3], 'w') as tmp:
    string = copy_head(styles_file, content_file)
    tmp.write(string)

