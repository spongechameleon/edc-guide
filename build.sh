#!/usr/bin/bash
# Generate html from the markdown file
pandoc edc_guide.md -f markdown -t html --quiet -s -o index.html.new

# Adjust the html via python helper scripts
# python <script name> <read file> <write file>
cd scripts

# Special case, combine existing styles with new content
python copy_head.py ../index.html ../index.html.new tmp.html

# Change all instances of '<a>some link</a>' to '<a target="_blank">some link</a>'
python add_target_blank_to_a_tags.py tmp.html tmp2.html
mv tmp2.html tmp.html

# Clean things up
cd ..
rm index.html.new
mv scripts/tmp.html index.html
