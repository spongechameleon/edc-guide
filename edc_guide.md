# EDC Guide 2023

Coming to [Electric Daisy Carnival (EDC)](https://lasvegas.electricdaisycarnival.com/) with the homies next year? Come on in, you're in the right place! This guide will attempt to explain the details of the festival.

## Schedule

Where

- Las Vegas, Nevada

When

- Friday, May 19 - Sunday, May 21

The festival is held at night and goes from 6PM - 5:30AM each night.

So, the festival kicks off on Friday night at 6PM and concludes on Monday morning at 5:30AM.

This means:

- Plan to be in Las Vegas by Friday afternoon (ideally by 4PM)
- Do NOT plan to leave Las Vegas until Monday morning at 10AM, at the earliest
- Plan to take Friday and Monday off of work at a minimum
- Recommend taking off on Tuesday

## Cost

- Ticket: $400
  - Valid for all 3 days of the festival (there are no single day passes)
  - Recommend getting GA, what the majority of the group will have
  - [Link for purchase](https://lasvegas.electricdaisycarnival.com/tickets/general-admission/)
- Shuttle pass: $100
  - Valid for all 3 days of the festival
  - The festival is held at Las Vegas Speedway 15 miles north of the strip
  - Traffic is very bad and what is normally a 20 minute drive will take 2-3 hours
  - EDC runs special shuttles that take a special route to skip traffic, only takes ~1 hour
  - Won't be on sale until March 2023, probably
- Lodging: $300
  - We are thinking of getting a huge airbnb, group size right now is ~25 people. More details to come in the following months.
- Flight: $400
- Food: $200

This comes out to $1,400 for the weekend.

## Ok but what do we do there

TLDR; eat -> sleep -> rave -> repeat

We dance, and dance, and dance, and dance... (sleep)... dance, dance, DANCE!!!

Typical day:

- 3PM, squad assembles for a large meal
- 5PM, get dressed
- 6PM, board the shuttle
- 8PM, arrive at festival
- Dance for 10 hours
- 6AM, board the return shuttle
- 8AM, go to bed

## ...dance for 10 hours???

There at 9 stages at EDC, along with a handful of smaller stages known as "art cars". The three largest stages host a variety of genres and all the big name DJs: Tiesto, Zedd, Martin Garrix, Deadmau5, etc...

The smaller stages are mostly genre specific:

- Wasteland -> hardstyle
- Basspod -> drum 'n bass
- Quantum Valley -> trance
- Neon Garden -> house
- Stereo Bloom -> house

Check out [last year's lineup](https://d3vhc53cl8e8km.cloudfront.net/hello-staging/wp-content/uploads/sites/21/2022/03/23200823/edclv_2022_mk_lu_full_teaser_1080x1350_r06.png) for a good idea of who shows up and this [spotify playlist](https://open.spotify.com/playlist/6aKeNXbP3MirExlGu1tVXf) for the type of tunes you'll hear

## Non-dancing activities

There are carnival rides at EDC! There's a ferris wheel, the flying chair thing, bumper cars, all types of stuff.

There is a place called EDC Town. In the past it has had a silent disco, a karaoke bar, a chapel for weddings and a bar operated by midgets.

There are some art installations scattered around the festival grounds, as well as designated chill spots to lay down on some grass and take a breather.

There is a fireworks show each night around 12:30AM. It's pretty great.

## How to survive

Must-have items:

- Earplugs
  - Highly recommend [downbeats](https://www.amazon.com/DownBeats-Reusable-Fidelity-Hearing-Protection/dp/B00A3Z44R2/ref=sr_1_2?crid=2JYSRO8WKJY77&keywords=downbeat&qid=1658522421&sprefix=downbeat%2Caps%2C126&sr=8-2) or something like them, you'll enjoy the music way more than you will with foam plugs!
- Backup earplugs
  - Seriously, you will annihilate your ears. Bring earplugs.
- Water bottle or hydration pack
  - There are free water stations around the festival grounds
- Comfortable shoes + comfortable insoles
  - You'll be dancing/standing/walking for about 10 hours in a row!! You probably won't notice any discomfort during the hubbub of the festival, but your feet and knees will thank you in the morning if you had some comfy kicks on
- Fanny pack or travel belt
  - [Example travel belt](https://www.amazon.com/Day-Tip-Money-Belt-Undercover/dp/B01M594B5K/ref=sr_1_4?keywords=travel+belt&qid=1658522660&sr=8-4)
  - Unfortunately, pickpocketing is common at EDC and squad members have been hit twice in the past three years
  - Keeping your pockets empty is the single best thing you can do to keep your possessions safe

Helpful actions:

- Timestamp your texts when at the festival
  - Texts may take a few hours to go through due to the large crowd, so manually typing out "im going to the food trucks, 10:45" will be less confusing when the text is finally delivered at 2AM
- Take a picture of the totem before the festival!
  - Since reception is really bad, we carry around a [totem](https://www.everfest.com/magazine/17-epic-totems-at-edc) to keep the group together

## How to thrive

Look, I think EDC is amazing- why else would I be writing a guide about it?! I don't think any amount of words, pictures, or [trailers](https://www.youtube.com/watch?v=GKKQuG9baDg) can prepare anyone for the raw emotions of being there.

But, I'll offer some advance words on how to get the most out of your EDC anyways.

EDC is big. There are ~166,000 people in attendance each night. There will be more than 20 people in our group.

**Expect not to see all the DJs you want to see.**

Each night, I pick one set that I can't miss and leave the rest of the night open. Only picking one set to see in a 10 hour window might seem ridiculously underkill but there's a lot going on! Over the four EDCs I've attended, I've had a better time soaking up the moment of whatever we've gotten ourselves into rather than stressing about checking off all the names on my DJ hit list.

Aside from different opinions in the group, a big reason to just pick a couple sets to see each night is that commuting between stages takes time. Expect long rave trains on the way in and out of stages, especially at mainstage (kinetic). Going across the speedway from kinetic -> circuit -> kinetic back-to-back is not ideal. Expect long lines during pit stops to the water stations and bathrooms too.

I think there's also a lot to be said for exploring new stages and artists. Letting loose and vibing at random art car sets where you don't even know who's up on stage is a unique experience, so take advantage!

**Be nice to other people, embody PLUR!**

As mentioned, there will be long rave trains at the big stages and it's going to take time to commute around the festival grounds. You can look at this as wasted time, or you can look at it as time to meet- albeit briefly- some of your fellow festival-goers.

Dancing with people as you walk by, complimenting people on their outfits and saying "sorry" and "excuse me" go a really long way towards making everyone's festival experience that much better. "You receive the energy you put out" or something like that, eh?

The fact is we'd all rather not be in a rave train, but they're an unavoidable part of festivals (insert gif of Tony Soprano saying "wuhugondo" here) and we might as well spend that time having fun with people rather than angrily rushing around throwing elbows just to make it to the stage one minute faster.

## Closing Remarks

I'm really really excited that you read this and are considering coming to EDC. I can't wait.

We should be sorting out lodging and the WFH week* in the next couple months.

*idea is to spend the week in Vegas and WFH from Monday - Thursday before the festival weekend begins
